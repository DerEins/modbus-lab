#!/bin/zsh

INSTALL_DIR=$PWD/install

mkdir -p $INSTALL_DIR

for VAR in modpoll diagslave
do
  curl https://www.modbusdriver.com/downloads/$VAR.tgz | tar xzvf - 
  mv $VAR $INSTALL_DIR
done
