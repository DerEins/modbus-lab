# Laboratoire d'exploration de trames ModBUS
## Sources
Ce laboratoire utilise deux produits open-source provenant de [Modbus Driver](https://modbusdriver.com):
- `modpoll` : Maître Modbus permettant de lancer des requêtes Modbus. 
- `diagslave` : Exclave Modbus permettant d'écouter et de répondre aux requêtes Modbus.

## Installation
L'installation locale du maître `modpoll` et de l'esclave `diagslave` est possible via le script Bash [`install.sh`](/install.sh).

Afin de faciliter l'utilisation de ces deux modules, il est recommandé d'ajouter les deux exécutables dans la variable d'environnement `$PATH` :
```bash
export PATH=$PWD/install/modpoll/x86_64-linux-gnu:$PATH
export PATH=$PWD/install/diagslave/x86_64-linux-gnu:$PATH
```

## Utilisation
### Client
L'esclave `diagslave` permet d'écouter des requêtes Modbus selon différents modes (décrits dans le menu d'aide de `diagslave` consultable via `diagslave -h`).
Une écoute en mode TCP peut s'effectuer via cette commande : 
```bash
sudo diagslave -m tcp -p 502
```
Options utilisées :
- `-m` : Choix du mode d'écoute. Ici, on utilise TCP.
- `-p` : Choix du port TCP d'écoute. Ici, on utilise le port 502, port standard de Modbus. L'utilisation de ce port (inférieur à 1024) nécessite l'exécution du client en tant qu'administrateur (ici avec `sudo`).

Une fois lancé, le client est en écoute permanante de requêtes Modbus sur le port 502. Il peut-être arrêté avec un simple `Ctrl+C`.

D'autres exemples d'utilisation sont disponivles [sur la page web dédié à `diagslave`](https://www.modbusdriver.com/diagslave.html)
### Serveur
Le maître `modpoll` permet de construire des requêtes Modbus et de les envoyer à un esclave donné (menu d'aide disponible via `modbus -h`. Pour ce laboratoire, on peut envoyer une requête Mudbus via TCP sur le port 502 à notre esclave :
```bash
modpoll -t4:float -r 100 -c 5 -1 127.0.0.1
```
Options utilisées
- `-t` : Format des données envoyées. Ici, on précise `4:float` pour envoyer des flottants sur 32 bits
- `-r` : Référence de départ pour les registres 
- `-c` : Nombre de valeur à lire
- `-1` : Envoi unique de la donnée (pas de boucle)

Ici, l'esclave à contacter est dans le réseau local, donc en `127.0.0.1`.

D'autres exemples sont disponibles [sur la page web dédié à `modpoll`](https://www.modbusdriver.com/modpoll.html)

## Analyse des paquets envoyés
### Vérification de la connexion Modbus
On peut alors renifler le traffic entre le maître et l'esclave avec un outils d'analyse de traffic réseau comme `Wireshark`.
On peut ainsi dans un premier temps vérifier que la communication entre les deux machines est bonne, ce qui est le cas d'après cette capture d'écran `Wireshark`

![Capture d'écran Wireshark d'une connexion Modbus](assets/capture.png)

On vérifie alors qu'il y a bien une requête/réponse Modbus qui est encapsulée dans TCP.

### Détails des paquets envoyés

||Requête Modbus|Réponse Modbus|
|:-:|:-:|:-:|
|Capture Wireshark|![Capture d'écran Wireshark d'une requête Modbus](assets/request.png)|![Capture d'écran Wireshark de la réponse à la requête Modbus](assets/response.png)|
|Remarques|La requête Modbus générée ne comporte que très peu d'informations : le registre de référence (celui donné dans `modpoll`  -1) et le nombre de données à lire (ici 10)|La réponse est elle-aussi simple avec quasi-uniquement que les registres demandés par la requête|

Les deux paquets Modbus comportent le même en-tête "Modbus/TCP" excepté la longueur du paquet qui change.
Dans tout les cas, on remarque qu'il n'y a aucun mécanisme de chiffrement ni d'authentification dans une connexion Modbus, ce qui rend ce protocole hautement vulnérable aux attaques par homme du milieu.
